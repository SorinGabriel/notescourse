Dynamic typed - la runtime, executa multe behavioruri de programare pe care programele statice le realizeaza la compilare.

byte - fiecare caracter ascii corespunde unui cod binar ...ca sa ma asigur ca nu-l transforma, e de tip byte

bytes - string - acelasi lucru, doar ca: 
la string - pot interpreta, transforma 
la byte - nu pot transforma

>>> CLI-> PYTHON [MOD INTERACTIV]

// -> impartire -> rezultatul e int 
/ -> impartile -> rezultatul e float


"is" e mult mai reliable pentru a compara strict tipuri de date fata de "=="
== in vechile versiuni de Python compara zona de memorie pe biti
is compara identitatea de clasa


operatori pe biti: shiftari pe st, dr, xor, sor 
0   1  0  0  1  1  1  0
128 64 32 16 8  4  2  1
1 cu 1 = 1 
1 cu 0 = 0		

opertori binari : pentru criptare, decriptare

10 << 1

Binar 10: 
1  0  1  0
8  4  2  1
8  0  2  0 

8 + 2 = 10

10 << 1:

1  0  1  0  0
16 8  4  2  1

16 0  4  0  0
16+4 =20 

10 << 1:

1  0  1  0  0  0
32 16 8  4  2  1
32 0  8  0  0  0
32 + 8 = 40 

10 >> 1: 
1  0 1  0  0  
16 8 4  2  1
20

cand fac 10 << 1: adaug un 0 in coada 
cand fac 10 >> 1: tai un 0 in coada

print(1 + 2 + 5 - (2 * 2)) != print(1 + 2 + 5 - 2 * 2) !!!!!
...ia mai intai ce e in paranteza, nu tine cont ca e adunare/ scadere sau inmultire/ impartire


code comment:
# (single line, de preferat)
/*    */
""" .... """ (multiline, de preferat)
"..."

coding standard (pip8): nu ar trebui sa punem prea mult cod pe un rand ..continuam pe 
urmatorul rand cu \ (windows) sau / (linux)


keyword - cuvinte rezervate de limbaj pe care nu le putem folosi decat in contextul definit de limbaj
None - ne da voie sa atribuim unei variabile valoarea null.
and - operator logic care returneaza o valoarea falsa sau true intre 2 operanzi
in - operator de incluziune ce returnaza o valoare de adevar sau fals 
is - operator de incluziune ce ne ajuta sa verificam validitatea unei valori
lambda - o functie care descrie o alta functie. Prin modul in care ii spun, ea va descrie alta functie.

[1,4,5,6,7,8]
X:X
X:X+1  ...itereaza peste tot si le afiseaza cu o unitate in plus
lambda merge doar pe functii continue

raise - ii spune programului ca executia trebuie sa se incheie aici
cast - schimbare de tip de variabila

UTF-8
- NU ARE caractere japoneze
- o incodare imi zice cu ce caractere pot lucra
utf-16 are caractere japoneze 
- python 3.x e incodat in utf-8 in mod predefinit
- 8 adica e pe 8biti

pycharm e mai optimizat pentru sistem ...si prioritizeaza eroarea (e un behavior)
